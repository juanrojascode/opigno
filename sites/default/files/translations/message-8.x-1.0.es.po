# Spanish translation of Message (8.x-1.0)
# Copyright (c) 2020 by the Spanish translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Message (8.x-1.0)\n"
"POT-Creation-Date: 2020-05-31 18:18+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Spanish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "Delete"
msgstr "Eliminar"
msgid "Author"
msgstr "Autor"
msgid "Description"
msgstr "Descripción"
msgid "Language"
msgstr "Idioma"
msgid "Enabled"
msgstr "Activado"
msgid "Edit"
msgstr "Editar"
msgid "Reset"
msgstr "Restablecer"
msgid "Message"
msgstr "Mensaje"
msgid "Weight"
msgstr "Peso"
msgid "Settings"
msgstr "Configuración"
msgid "Name"
msgstr "Nombre"
msgid "Label"
msgstr "Etiqueta"
msgid "View"
msgstr "Ver"
msgid "Text"
msgstr "Texto"
msgid "Created"
msgstr "Creado"
msgid "Page"
msgstr "Página"
msgid "Delete messages"
msgstr "Eliminar mensajes"
msgid "Delta"
msgstr "Delta"
msgid "Message text"
msgstr "Texto del mensaje"
msgid ", "
msgstr ", "
msgid "Desc"
msgstr "Desc"
msgid "Anonymous"
msgstr "Anónimo"
msgid "Arguments"
msgstr "Argumentos"
msgid "Template"
msgstr "Plantilla"
msgid "Sort by"
msgstr "Ordenar por"
msgid "message"
msgstr "mensaje"
msgid "Messages"
msgstr "Mensajes"
msgid "‹ previous"
msgstr "‹ anterior"
msgid "next ›"
msgstr "siguiente ›"
msgid "Node title"
msgstr "Título del nodo"
msgid "Days"
msgstr "Días"
msgid "Date created"
msgstr "Fecha de creación"
msgid "Quota"
msgstr "Cuota"
msgid "Message Text"
msgstr "Texto del mensaje"
msgid "Apply"
msgstr "Aplicar"
msgid "Add another item"
msgstr "Añadir otro elemento"
msgid "Language code"
msgstr "Código de idioma"
msgid "« first"
msgstr "« primero"
msgid "last »"
msgstr "último »"
msgid "Offset"
msgstr "Desplazamiento"
msgid "Field settings"
msgstr "Opciones del campo"
msgid "Entity type"
msgstr "Tipo de entidad"
msgid "Created by"
msgstr "Creado por"
msgid "Created on"
msgstr "Creado en"
msgid "Message templates"
msgstr "Plantillas de Mensajes"
msgid "Items per page"
msgstr "Elementos por página"
msgid "- All -"
msgstr "- Todo -"
msgid "Machine-readable name"
msgstr "Nombre de sistema"
msgid "Author name"
msgstr "Nombre del autor"
msgid "Published date"
msgstr "Fecha de publicación"
msgid "Message settings"
msgstr "Configuración de los mensajes"
msgid "Message template"
msgstr "Plantilla de mensaje"
msgid "Delete selected content"
msgstr "Eliminar el contenido seleccionado"
msgid "Are you sure you want to delete this item?"
msgid_plural "Are you sure you want to delete these items?"
msgstr[0] "¿Seguro que quiere borrar este elemento?"
msgstr[1] "¿Seguro que quiere borrar estos elementos?"
msgid "Full content"
msgstr "Contenido completo"
msgid "Tools that enhance the user interface."
msgstr "Herramientas que mejoran la interfaz de usuario."
msgid "UUID"
msgstr "UUID"
msgid "Weight for @title"
msgstr "Peso para @title"
msgid "Weight for row @number"
msgstr "Peso para la fila @number"
msgid "Message ID"
msgstr "ID de mensaje"
msgid "Asc"
msgstr "Asc"
msgid "Master"
msgstr "Máster"
msgid "Replace tokens"
msgstr "Reemplazar tokens"
msgid "With selection"
msgstr "Con selección"
msgid "The message UUID."
msgstr "El UUID del mensaje."
msgid "The message language code."
msgstr "El código de idioma del mensaje."
